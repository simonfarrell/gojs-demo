// "Global" variables

var myDiagram = null;
var cxElement = null;


// This is the general menu command handler, parameterized by the name of the command.
// It gets called by clicks on HTML elements in the page
function cxcommand(event, val) {
    if (val === undefined) val = event.currentTarget.id;
    var diagram = myDiagram;
    switch (val) {
      case "newCloudPort": addNode("Cloud Port"); break;
      case "newCloudGW": addNode("Cloud Gateway"); break;
      case "newCustomerPort": addNode("Customer Port"); break;
      
    }
    diagram.currentTool.stopTool();
  }
  
  
// This is the main setup function. It gets called on window.load
function init() {
    // This is the actual HTML context menu:
    cxElement = document.getElementById("contextMenu");

    var $ = go.GraphObject.make;  // for conciseness in defining templates

    myDiagram = makeDiagram($);

    ///////////////// Adding custom context menu ////////

    // Since we have only one main element, we don't have to declare a hide method,
    // we can set mainElement and GoJS will hide it automatically
    var myContextMenu = $(go.HTMLInfo, {
        show: showContextMenu,
        hide: hideContextMenu
    });

    myDiagram.contextMenu = myContextMenu;

    // We don't want the div acting as a context menu to have a (browser) context menu!
    cxElement.addEventListener("contextmenu", function(e) {
      e.preventDefault();
      return false;
    }, false);

    // a context menu is an Adornment with a bunch of buttons in them
    var partContextMenu = makePartContextMenu($);

    // Make templates for boxes and lines
    //myDiagram.nodeTemplate = makeCloudPortNode($, partContextMenu);
    makeNodeTemplateMap(myDiagram, $, partContextMenu);

    myDiagram.linkTemplate = makeLinkTemplate($, partContextMenu);

    // Groups consist of a title in the color given by the group node data
    // above a translucent gray rectangle surrounding the member parts
    myDiagram.groupTemplate = makeGroupTemplate($, partContextMenu);

    // provide a tooltip for the background of the Diagram, when not over any Part
    myDiagram.toolTip = makeBackgroundTooltip($);

    // provide a context menu for the background of the Diagram, when not over any Part
 //   myDiagram.contextMenu = makeBGContextMenu($);
  
    // Create the Diagram's Model:
    myDiagram.model = new go.GraphLinksModel(makeInitialNodes(), makeInitialLinks());

}

function addNode(nodeType) {
    myDiagram.startTransaction();
    myDiagram.model.addNodeData({category: nodeType, text: nodeType})
    myDiagram.commitTransaction();
}

function makeDiagram($) {
    return $(go.Diagram, "myDiagramDiv",  // create a Diagram for the DIV HTML element
    {
      // allow double-click in background to create a new node
      "clickCreatingTool.archetypeNodeData": { text: "Node", color: "white" },

      // allow Ctrl-G to call groupSelection()
      "commandHandler.archetypeGroupData": { text: "Group", isGroup: true, color: "blue" },

      // enable undo & redo
      "undoManager.isEnabled": true
    });
}

/////////// Model related functions ////////////
function makeInitialNodes() {
    return [
    { key: 1, geo: "home", category: "Customer Port", text: "Home Base", color: "lightblue", loc: "0 0" },
    { key: 2, geo: "droplet", category: "Cloud Gateway", text: "Cloud GW", color: "yellow", loc: "300 50" }
  //   { key: 3, text: "Gamma", color: "lightgreen", group: 5 },
  //   { key: 4, text: "Delta", color: "pink", group: 5 },
  //   { key: 5, text: "Epsilon", color: "green", isGroup: true }
  ];
}

function makeInitialLinks() {
      return [
    { from: 1, to: 2, color: "blue", text: "100Mbps" }
  //   { from: 2, to: 2 },
  //   { from: 3, to: 4, color: "green" },
  //   { from: 3, to: 1, color: "purple" }
  ];
}

function canAddCloudPort() {
    return true;
}

function canAddCloudGW() {
    return true;
}

function canAddCustomerPort() {
    return true;
}

/////////// Node-related functions ////////////

function nodeInfo(d) {  // Tooltip info for a node data object
  var str = "Node " + d.key + ": " + d.text + "\n";
  if (d.group)
    str += "virtual member of " + d.group;
  else
    str += "top-level node";
  return str;
}

function geoFunc(geoname) {
    var geo = icons[geoname];
    if(geo === undefined) geo = icons["heart"];
    if(typeof geo === "string") {
        geo = icons[geoname] = go.Geometry.parse(geo, true);
    }
    return geo;
}

function makeSVGNode($, partContextMenu) {
    return $(go.Node, "Auto",
        $(go.Shape, "Circle",
            {fill: "lightcoral", strokeWidth:0, width: 65, height: 65},
            new go.Binding("fill", "color")),
        $(go.Shape, 
            {margin: 3, fill: "#1cc1bc", strokeWidth: 0},
            new go.Binding("geometry","geo", geoFunc)),
        {
            toolTip:
                $("ToolTip",
                    { "Border.stroke": "#4444", "Border.strokeWidth": 2 },
                    $(go.TextBlock, {margin: 8, stroke: "#444444", font: "bold 16px sans-serif"},
                        new go.Binding("text", "geo")))
        }
    );
}

function makeCloudPortNode($, partContextMenu) {
    return $(go.Node, "Auto",
    new go.Binding("location", "loc", go.Point.parse),
    // { locationSpot: go.Spot.Center },
    $(go.Shape, "RoundedRectangle",
      {
        fill: "orange", // the default fill, if there is no data bound value
        portId: "", cursor: "pointer",  // the Shape is the port, not the whole Node
        // allow all kinds of links from and to this port
        fromLinkable: true, fromLinkableSelfNode: false, fromLinkableDuplicates: false,
        toLinkable: true, toLinkableSelfNode: false, toLinkableDuplicates: false
      },
      new go.Binding("fill", "color")),
    $(go.TextBlock,
      {
        font: "bold 14px sans-serif",
        stroke: '#333',
        margin: 6,  // make some extra space for the shape around the text
        isMultiline: false,  // don't allow newlines in text
        editable: true  // allow in-place editing by user
      },
      new go.Binding("text", "text").makeTwoWay()),  // the label shows the node data's text
    { // this tooltip Adornment is shared by all nodes
      toolTip:
        $("ToolTip",
          $(go.TextBlock, { margin: 4 },  // the tooltip shows the result of calling nodeInfo(data)
            new go.Binding("text", "", nodeInfo))
        ),
      // this context menu Adornment is shared by all nodes
      contextMenu: partContextMenu
    }
  );
}

function makeCloudGatewayNode($, partContextMenu) {
    return $(go.Node, "Auto",
    new go.Binding("location", "loc", go.Point.parse),
    // { locationSpot: go.Spot.Center },
    $(go.Shape, "Rectangle",
      {
        fill: "white", // the default fill, if there is no data bound value
        portId: "", cursor: "pointer",  // the Shape is the port, not the whole Node
        // allow all kinds of links from and to this port
        fromLinkable: true, fromLinkableSelfNode: false, fromLinkableDuplicates: false,
        toLinkable: true, toLinkableSelfNode: false, toLinkableDuplicates: false
      },
      new go.Binding("fill", "color")),
    $(go.TextBlock,
      {
        font: "bold 14px sans-serif",
        stroke: '#333',
        margin: 6,  // make some extra space for the shape around the text
        isMultiline: false,  // don't allow newlines in text
        editable: true  // allow in-place editing by user
      },
      new go.Binding("text", "text").makeTwoWay()),  // the label shows the node data's text
    { // this tooltip Adornment is shared by all nodes
      toolTip:
        $("ToolTip",
          $(go.TextBlock, { margin: 4 },  // the tooltip shows the result of calling nodeInfo(data)
            new go.Binding("text", "", nodeInfo))
        ),
      // this context menu Adornment is shared by all nodes
      contextMenu: partContextMenu
    }
  );
}

function makeCustomerPortNode($, partContextMenu) {
    return $(go.Node, "Auto",
    new go.Binding("location", "loc", go.Point.parse),
    // { locationSpot: go.Spot.Center },
    $(go.Shape, "Circle",
      {
        fill: "blue", // the default fill, if there is no data bound value
        portId: "", cursor: "pointer",  // the Shape is the port, not the whole Node
        // allow all kinds of links from and to this port
        fromLinkable: true, fromLinkableSelfNode: false, fromLinkableDuplicates: false,
        toLinkable: true, toLinkableSelfNode: false, toLinkableDuplicates: false
      },
      new go.Binding("fill", "color")),
    $(go.TextBlock,
      {
        font: "bold 14px sans-serif",
        stroke: '#333',
        margin: 6,  // make some extra space for the shape around the text
        isMultiline: false,  // don't allow newlines in text
        editable: true  // allow in-place editing by user
      },
      new go.Binding("text", "text").makeTwoWay()),  // the label shows the node data's text
    { // this tooltip Adornment is shared by all nodes
      toolTip:
        $("ToolTip",
          $(go.TextBlock, { margin: 4 },  // the tooltip shows the result of calling nodeInfo(data)
            new go.Binding("text", "", nodeInfo))
        ),
      // this context menu Adornment is shared by all nodes
      contextMenu: partContextMenu
    }
  );
}

function makeNodeTemplateMap(myDiagram, $, partContextMenu) {
    myDiagram.nodeTemplateMap.add("", // the default category
        makeCloudPortNode($, partContextMenu)
    );
    myDiagram.nodeTemplateMap.add("Cloud Port", makeCloudPortNode($, partContextMenu));
    myDiagram.nodeTemplateMap.add("Cloud Gateway", makeCloudGatewayNode($, partContextMenu));
    myDiagram.nodeTemplateMap.add("Customer Port", makeCustomerPortNode($, partContextMenu));

}

////////// Link-related functions //////////////

// Define the appearance and behavior for Links:
function linkInfo(d) {  // Tooltip info for a link data object
  return "Link:\nfrom " + d.from + " to " + d.to;
}

function makeLinkTemplate($, partContextMenu) {
  return $(go.Link,
    { toShortLength: 3, relinkableFrom: true, relinkableTo: true },  // allow the user to relink existing links
    $(go.Shape,
      { strokeWidth: 2 },
      new go.Binding("stroke", "color")),
    $(go.TextBlock, {margin: 5},
        new go.Binding("text", "text")),
    { // this tooltip Adornment is shared by all links
      toolTip:
        $("ToolTip",
          $(go.TextBlock, { margin: 4 },  // the tooltip shows the result of calling linkInfo(data)
            new go.Binding("text", "", linkInfo))
        ),
      // the same context menu Adornment is shared by all links
      contextMenu: partContextMenu
    }
  );

}

/////////////////////// Context menu and background click functions ////////////////

// To simplify this code we define a function for creating a context menu button:
function makeButton($, text, action, visiblePredicate) {
    return $("ContextMenuButton",
        $(go.TextBlock, text),
        { click: action },
        // don't bother with binding GraphObject.visible if there's no predicate
        visiblePredicate ? new go.Binding("visible", "", function(o, e) { return o.diagram ? visiblePredicate(o, e) : false; }).ofObject() : {}
    );
}

function makePartContextMenu($) {
    return go.GraphObject.make("ContextMenu",
        makeButton($, "Properties",
        function(e, obj) {  // OBJ is this Button
            var contextmenu = obj.part;  // the Button is in the context menu Adornment
            var part = contextmenu.adornedPart;  // the adornedPart is the Part that the context menu adorns
            // now can do something with PART, or with its data, or with the Adornment (the context menu)
            if (part instanceof go.Link) alert(linkInfo(part.data));
            else if (part instanceof go.Group) alert(groupInfo(contextmenu));
            else alert(nodeInfo(part.data));
        }),
        makeButton($,"Cut",
        function(e, obj) { e.diagram.commandHandler.cutSelection(); },
        function(o) { return o.diagram.commandHandler.canCutSelection(); }),
        makeButton($,"Copy",
        function(e, obj) { e.diagram.commandHandler.copySelection(); },
        function(o) { return o.diagram.commandHandler.canCopySelection(); }),
        makeButton($,"Paste",
        function(e, obj) { e.diagram.commandHandler.pasteSelection(e.diagram.toolManager.contextMenuTool.mouseDownPoint); },
        function(o) { return o.diagram.commandHandler.canPasteSelection(o.diagram.toolManager.contextMenuTool.mouseDownPoint); }),
        makeButton($,"Delete",
        function(e, obj) { e.diagram.commandHandler.deleteSelection(); },
        function(o) { return o.diagram.commandHandler.canDeleteSelection(); }),
        makeButton($,"Undo",
        function(e, obj) { e.diagram.commandHandler.undo(); },
        function(o) { return o.diagram.commandHandler.canUndo(); }),
        makeButton($,"Redo",
        function(e, obj) { e.diagram.commandHandler.redo(); },
        function(o) { return o.diagram.commandHandler.canRedo(); }),
        makeButton($,"Group",
        function(e, obj) { e.diagram.commandHandler.groupSelection(); },
        function(o) { return o.diagram.commandHandler.canGroupSelection(); }),
        makeButton($,"Ungroup",
        function(e, obj) { e.diagram.commandHandler.ungroupSelection(); },
        function(o) { return o.diagram.commandHandler.canUngroupSelection(); })
    );
}

function makeBGContextMenu($) {
    return $("ContextMenu",
        makeButton($, "Paste",
            function(e, obj) { e.diagram.commandHandler.pasteSelection(e.diagram.toolManager.contextMenuTool.mouseDownPoint); },
            function(o) { return o.diagram.commandHandler.canPasteSelection(o.diagram.toolManager.contextMenuTool.mouseDownPoint); }),
        makeButton($, "Undo",
            function(e, obj) { e.diagram.commandHandler.undo(); },
            function(o) { return o.diagram.commandHandler.canUndo(); }),
        makeButton($, "Redo",
            function(e, obj) { e.diagram.commandHandler.redo(); },
            function(o) { return o.diagram.commandHandler.canRedo(); })
    );
}

function hideCX() {
    if (myDiagram.currentTool instanceof go.ContextMenuTool) {
      myDiagram.currentTool.doCancel();
    }
}

function showContextMenu(obj, diagram, tool) {
    // Show only the relevant buttons given the current state.
    var cmd = diagram.commandHandler;
    var hasMenuItem = false;
    function maybeShowItem(elt, pred) {
      if (pred) {
        elt.style.display = "block";
        hasMenuItem = true;
      } else {
        elt.style.display = "none";
      }
    }
    maybeShowItem(document.getElementById("newCloudPort"), canAddCloudPort());
    maybeShowItem(document.getElementById("newCloudGW"), canAddCloudGW());
    maybeShowItem(document.getElementById("newCustomerPort"), canAddCustomerPort());

    // Now show the whole context menu element
    if (hasMenuItem) {
        cxElement.classList.add("show-menu");
        // we don't bother overriding positionContextMenu, we just do it here:
        var mousePt = diagram.lastInput.viewPoint;
        cxElement.style.left = mousePt.x + 5 + "px";
        cxElement.style.top = mousePt.y + "px";
    }

    // Optional: Use a `window` click listener with event capture to
    //           remove the context menu if the user clicks elsewhere on the page
    window.addEventListener("click", hideCX, true);
}

function hideContextMenu() {
    cxElement.classList.remove("show-menu");
    // Optional: Use a `window` click listener with event capture to
    //           remove the context menu if the user clicks elsewhere on the page
    window.removeEventListener("click", hideCX, true);
}

// Define the behavior for the Diagram background:
function diagramInfo(model) {  // Tooltip info for the diagram's model
    return "Model:\n" + model.nodeDataArray.length + " nodes, " + model.linkDataArray.length + " links";
}

function makeBackgroundTooltip($) {
    return $("ToolTip",
        $(go.TextBlock, { margin: 4 },
        new go.Binding("text", "", diagramInfo))
    );
}

/////////////////////// Group related functions ////////////////

    // Define the appearance and behavior for Groups:
function groupInfo(adornment) {  // takes the tooltip or context menu, not a group node data object
    var g = adornment.adornedPart;  // get the Group that the tooltip adorns
    var mems = g.memberParts.count;
    var links = 0;
    g.memberParts.each(function(part) {
        if (part instanceof go.Link) links++;
    });
    return "Group " + g.data.key + ": " + g.data.text + "\n" + mems + " members including " + links + " links";
}

function makeGroupTemplate($, partContextMenu) {
    return $(go.Group, "Vertical",
    {
    selectionObjectName: "PANEL",  // selection handle goes around shape, not label
    ungroupable: true  // enable Ctrl-Shift-G to ungroup a selected Group
    },
    $(go.TextBlock,
    {
        //alignment: go.Spot.Right,
        font: "bold 19px sans-serif",
        isMultiline: false,  // don't allow newlines in text
        editable: true  // allow in-place editing by user
    },
    new go.Binding("text", "text").makeTwoWay(),
    new go.Binding("stroke", "color")),
    $(go.Panel, "Auto",
    { name: "PANEL" },
    $(go.Shape, "Rectangle",  // the rectangular shape around the members
        {
        fill: "rgba(128,128,128,0.2)", stroke: "gray", strokeWidth: 3,
        portId: "", cursor: "pointer",  // the Shape is the port, not the whole Node
        // allow all kinds of links from and to this port
        fromLinkable: true, fromLinkableSelfNode: false, fromLinkableDuplicates: false,
        toLinkable: true, toLinkableSelfNode: false, toLinkableDuplicates: false
        }),
    $(go.Placeholder, { margin: 10, background: "transparent" })  // represents where the members are
    ),
    { // this tooltip Adornment is shared by all groups
    toolTip:
        $("ToolTip",
        $(go.TextBlock, { margin: 4 },
            // bind to tooltip, not to Group.data, to allow access to Group properties
            new go.Binding("text", "", groupInfo).ofObject())
        ),
    // the same context menu Adornment is shared by all groups
    contextMenu: partContextMenu
    }
);
}